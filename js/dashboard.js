const user = {
	'name': 'Alessandro',
	'email': 'alessandro.vncs@gmail.com',
	'cpf': '0000000000',
	'op': 0,
	'password': 'test'
};

const menu = [
	{'pageUrl': 'home.html', 'pageName': 'Início', 'pageIcon': 'fas fa-home','pagePermission': [0,1,2]},
	{'pageUrl': 'environment.html','pageName': 'Ambiente','pageIcon': 'fas fa-map','pagePermission': [0]},
	{'pageUrl': 'users.html','pageName': 'Usuário','pageIcon': 'fas fa-users','pagePermission': [0,1]},
	{'pageUrl': 'devices.html','pageName': 'Dispositivos','pageIcon': 'fas fa-wifi','pagePermission': [0,2]},
	{'pageUrl': 'report.html','pageName': 'Relatório','pageIcon': 'fas fa-file-invoice','pagePermission': [0,1,2]},
	{'pageUrl': 'event.html','pageName': 'Evento','pageIcon': 'fas fa-calendar-times','pagePermission': [0,1,2]},
];

$(document).ready(function () {
	$('.page').click(function () { sideAction('hide'); });
	$('.fas.fa-bars').click(function () { sideAction('toggle'); });
	createMenu();
});

function sideAction(action){
	let sidebar = $('.sidebar').eq(0);

	if(action === 'hide'){
		sidebar.css('left',-1 * sidebar.outerWidth() - 30);
	}

	if(action === 'show'){
		sidebar.css('left',0);
	}

	if( action === 'toggle' && sidebar.css('left') === '0px'){
		sidebar.css('left',-1 * sidebar.outerWidth() - 30);
	} else if(action === 'toggle') {
		sidebar.css('left',0);
	}
}



function createMenu() {
	let sidebar = $('.sidebar-body');

	sidebar.html("");

	menu.map(function (arr) {
		if(arr.pagePermission.includes(user.op)){
			sidebar.append(
				"<div>" +
					"<i class='" + arr.pageIcon + "'></i>" +
					"<p>" + arr.pageName + "</p>" +
				"</div>"
			).find('div').last().click(function () {
				$('.page section').load('dashboard/pages/'+arr.pageUrl);
				sidebar.find('div').removeClass('active');
				$(this).addClass('active');
				loadScreen('show');
				$('.page p').text('Dashboard | ' + arr.pageName);
			});
		}
	});

	sidebar.find('div').eq(0).trigger('click');
}

function loadScreen(action){
	if(action === "show"){
		if($('.loading').is(":visible")) return;
		$('.body').append("<div class='loading'><p>Carregando...</p></div>");
	} else if(action === "hide"){
		$('.loading').remove();
	}
}

$('.sidebar-header').click(function () {
	sendMessage("oi oi oi oi oi oi oi oi oi oi oi oi o ioi oi oi oi oi oi oi ", "danger");
});