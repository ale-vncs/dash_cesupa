let count = 0;

$(document).ready(function () {
    $('#messageSide button p').text(count);
});


function sendMessage(msg, model = "success") { //Metodo para mandar alguma mensagem de alerta no dashboard
    let time = new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds();

    if(!$('#messageBox').is(':visible')){
        $(document.body).append("<div id='messageBox'></div>");
    }
    let mdl = "<li class='" + model + "'><p>" + time + "</p><p>" + msg + "</p></li>";
    let box = $(mdl);
    $("#messageBox").append(box);
    box.height(box.outerHeight(true));
    setTimeout(function() {
        box.css('height','0');
        setTimeout(function () {
            box.remove();
        },800);
    }, 5000);

    box.click(function () {
        $(this).css('height','0');
    });

    if($('#messageSide').is(':visible')){
        $('#messageContainer').append(mdl);
        ++count;
        if(count > 0){
            $('#messageSide button p').css('display','block').text(count);
        }
    }
    console.log(count);
}

$('#messageSide > button').click(function () {
    let msgSide = $('#messageSide');
    $(this).toggleClass('active');
    $('#messageSide button p').css('display','none').text(count = 0);
    if(msgSide.css('right') === '0px'){
        msgSide.css('right',-1 * $('#messageContainer').outerWidth() -20);
    } else {
        msgSide.css('right',0);
    }
});

