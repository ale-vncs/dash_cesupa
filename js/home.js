var config_gp_co2, gp_co2;

$(document).ready(function () {

	gp_co2 = new Chart($('#graph_co2'), config_gp_co2);
	loadScreen('hide');

	graphVisit(data);
	moment.locale('pt-BR');
	moment().format('L');
	moment.defaultFormat = "DD/MM/YYYY hh:mm:ss";
});

var data = [
	{
		"roomName": "Sala 001",
		"data":[
			{"time":"10/09/2019 11:53", "co2": "667" },
			{"time":"10/09/2019 11:58", "co2": "687" },
			{"time":"10/09/2019 12:03", "co2": "627" },
			{"time":"10/09/2019 12:08", "co2": "647" },
			{"time":"10/09/2019 12:13", "co2": "687" },
			{"time":"10/09/2019 12:18", "co2": "727" },
			{"time":"10/09/2019 12:23", "co2": "824" },
			{"time":"10/09/2019 12:28", "co2": "914" },
			{"time":"10/09/2019 12:33", "co2": "951" },
		]
	},
	{
		"roomName": "Sala 101",
		"data":[
			{"time":"10/09/2019 11:49", "co2": "267" },
			{"time":"10/09/2019 11:54", "co2": "387" },
			{"time":"10/09/2019 11:59", "co2": "527" },
			{"time":"10/09/2019 12:04", "co2": "817" },
			{"time":"10/09/2019 12:09", "co2": "677" },
			{"time":"10/09/2019 12:14", "co2": "427" },
		]
	},
];


function graphVisit(data) {
	data.map(function (arr, index) {
		config_gp_co2.data.labels = [new Date('10/09/2019 11:45')];
		config_gp_co2.data.datasets.push(
			{
				label: arr.roomName,
				data: arr.data.map(function (arr) {
					return {t: new Date(arr.time), y: arr.co2};
				}),
				backgroundColor: graphBackgroundColor[index],
				borderColor: graphBorderColor[index],
				borderWidth: 1,
				fill: false,
			}
		);
		//console.log(config_gp_co2.data.datasets[index]);
	});
	gp_co2.update();
}



config_gp_co2 = {
	type: 'line',
	data: {
		labels: [],
		datasets: []
	},
	options: {
		//responsive: true,
		legend: {
			display: true,
			labels: {
				fontColor: graphFontColor,
				fontSize: graphFontSizeLegend
			}
		},
		title: {
			display: true,
			text: 'Nivel de CO2',
			fontColor: graphFontColor,
			fontSize: graphFontSizeTitle
		},
		scales: {
			xAxes: [{
				ticks: {
					fontColor: graphFontColor,
				},
				type: 'time',
				time: {
					parser: 'DD/MM/YYYY HH:mm',
					displayFormats: {
						quarter: 'MMM YYYY'
					},
					tooltipFormat: 'DD/MM/YYYY HH:mm'
				},
				scaleLabel: {
					display: true,
					labelString: 'Horário'
				}
			}],
			yAxes: [{
				ticks: {
					fontColor: graphFontColor,
					callback: function(value, index, values) {
						return value + ' ppm';
					}
				},
			}]
		},
	}
};
