
$(document).ready(function () {
	$('.main').load("dashboard/dashboard.html");
});


const graphColor = [ //rgb das cores do grafico
	'rgb(255, 99, 132)',
	'rgb(54, 162, 235)',
	'rgb(255, 206, 86)',
	'rgb(75, 192, 192)',
	'rgb(153, 102, 255)',
	'rgb(255, 159, 64)',
	'rgb(155, 205, 255)',
	'rgb(80, 255, 109)',
	'rgb(255, 13, 250)',
	'rgb(255, 1, 60)',
	'rgb(255, 237, 16)',
	'rgb(231, 186, 255)',
];



const graphBackgroundColor = [];
const graphBorderColor = [];
const graphFontColor = 'rgb(0, 0, 0)'; //Cor do texto do grafico
const graphFontSizeLegend = 11;
const graphFontSizeTitle = 14;
const graphLegendPosition = 'right';

for(let i = 0; i < graphColor.length; i++){
	let reg = /[\d+\s*,]*/g;
	graphBackgroundColor.push('rgba( ' + graphColor[i].match(reg)[4] + ',0.3)');
	graphBorderColor.push('rgba( ' + graphColor[i].match(reg)[4] + ',1)');
}